## Installation Guide

1. Clone this repository.
2. Run `pod install`. **NOTE**: Please make sure that you're using the latest version of Cocoapods. At the time of this writing, the latest version is 1.7.5.
3. Open the _WeatherApp.xcworkspace_ and click `Run`.


## Features found in the app

So we have the minimum feature required which is fetching a 5-day forecast for a city. In addition to this: I decided to implement the following features:

* Master/detail interface
* City search
* Displaying icons
* Favorite cities
* Temperature localization
* Using the phone’s location to detect the default city
* Offline city search, so you don’t have to hit the slow API when searching


## Features I would like to implement

* Unit/UI testing
* Display map that will show the selected city
* Full localization support
* Edit the "Favourites" list
* Better UI for better UX


## Frameworks/Libraries used

* CoreData
* CoreLocation
* Alamofire
* Kingfisher
* JGProgressHUD


## Architecture

I used the VIP -- _View_, _Interactor_, _Presenter_ -- architecture.
