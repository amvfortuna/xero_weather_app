//
//  City+CoreDataProperties.swift
//  WeatherApp
//
//  Created by Anna Fortuna on 19/08/19.
//  Copyright © 2019 Anna Fortuna. All rights reserved.
//
//

import Foundation
import CoreData


extension City {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<City> {
        return NSFetchRequest<City>(entityName: "City")
    }

    @NSManaged public var country: String?
    @NSManaged public var favourite: Bool
    @NSManaged public var id: Int64
    @NSManaged public var name: String?
    @NSManaged public var longitude: Double
    @NSManaged public var latitude: Double

}
