//
//  City+CoreDataClass.swift
//  WeatherApp
//
//  Created by Anna Maria Fortuna on 16/08/19.
//  Copyright © 2019 Anna Fortuna. All rights reserved.
//
//

import Foundation
import CoreData


public extension CodingUserInfoKey {
    static let context = CodingUserInfoKey(rawValue: "context")
}


@objc(City)
public class City: NSManagedObject, Codable {
    
    enum CityCodingKeys: String, CodingKey {
        case
        id = "id",
        name = "name",
        country = "country",
        favourite = "favourite",
        coord = "coord"
        
        enum CoordCodingKeys: String, CodingKey {
            case
            longitude = "lon",
            latitude = "lat"
        }
    }
    
    
    class func cityFetchRequest() -> NSFetchRequest<City> {
        return City.fetchRequest()
    }
    
    
    public required convenience init(from decoder: Decoder) throws {
        guard let contextUserInfoKey = CodingUserInfoKey.context,
              let managedObjectContext = decoder.userInfo[contextUserInfoKey] as? NSManagedObjectContext,
              let entity = NSEntityDescription.entity(forEntityName: "City", in: managedObjectContext)  else {
            
                print("CodingUserInfoKey.context: \(String(describing: CodingUserInfoKey.context))")
                print("managedObjectContext: \(String(describing: decoder.userInfo[CodingUserInfoKey.context!] as? NSManagedObjectContext))")
                fatalError("Unable to decode City")
        }
        
        self.init(entity: entity, insertInto: managedObjectContext)
        
        let container = try decoder.container(keyedBy: CityCodingKeys.self)
        self.id = try container.decode(Int64.self, forKey: .id)
        self.name = try container.decodeIfPresent(String.self, forKey: .name)
        self.country = try container.decodeIfPresent(String.self, forKey: .country)
        
        let coordContainer = try container.nestedContainer(keyedBy: CityCodingKeys.CoordCodingKeys.self, forKey: .coord)
        self.longitude = try coordContainer.decode(Double.self, forKey: .longitude)
        self.latitude = try coordContainer.decode(Double.self, forKey: .latitude)
    }
    
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CityCodingKeys.self)
        try container.encode(self.id, forKey: .id)
        try container.encode(self.name, forKey: .name)
        try container.encode(self.country, forKey: .country)
        try container.encode(self.favourite, forKey: .favourite)
        
        var coordContainer = container.nestedContainer(keyedBy: CityCodingKeys.CoordCodingKeys.self, forKey: .coord)
        try coordContainer.encode(self.longitude, forKey: .longitude)
        try coordContainer.encode(self.latitude, forKey: .latitude)
    }
    
    
    static func == (lhs: City, rhs: City) -> Bool {
        return lhs.id == rhs.id && lhs.name == rhs.name && lhs.country == rhs.country
    }
}


