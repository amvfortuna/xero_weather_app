//
//  UISplitViewControllerHelper.swift
//  WeatherApp
//
//  Created by Anna Fortuna on 19/08/19.
//  Copyright © 2019 Anna Fortuna. All rights reserved.
//

import UIKit
import JGProgressHUD


extension UISplitViewController {
    
    static fileprivate var hud: JGProgressHUD = {
        let hud = JGProgressHUD(style: .dark)
        return hud
    }()
    
    
    func displayHUD(with message: String? = nil) {
        UISplitViewController.hud.indicatorView = JGProgressHUDIndeterminateIndicatorView()
        UISplitViewController.hud.textLabel.text = message ?? "Loading"
        UISplitViewController.hud.show(in: self.view, animated: true)
    }
    
    func hideHUD(delay: TimeInterval = 0) {
        UISplitViewController.hud.dismiss(afterDelay: delay, animated: true)
    }
    
    
    func displaySuccessHUD(with message: String) {
        UISplitViewController.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
        UISplitViewController.hud.textLabel.text = message
        UISplitViewController.hud.show(in: self.view, animated: true)
        self.hideHUD(delay: 2.0)
    }
}


