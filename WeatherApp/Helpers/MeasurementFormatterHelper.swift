//
//  MeasurementFormatterHelper.swift
//  WeatherApp
//
//  Created by Anna Maria Fortuna on 18/08/19.
//  Copyright © 2019 Anna Fortuna. All rights reserved.
//

import Foundation


extension MeasurementFormatter {
    
    static var defaultFormat: MeasurementFormatter = {
        let formatter = MeasurementFormatter()
        formatter.locale = Locale.current
        formatter.numberFormatter.maximumFractionDigits = 2
        return formatter
    }()
}


