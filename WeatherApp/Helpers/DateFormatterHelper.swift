//
//  DateFormatterHelper.swift
//  WeatherApp
//
//  Created by Anna Maria Fortuna on 18/08/19.
//  Copyright © 2019 Anna Fortuna. All rights reserved.
//

import Foundation


enum DateFormatterType {
    case
    dateTime,
    dateOnly,
    timeOnly
}


extension DateFormatter {
    
    convenience init(_ type: DateFormatterType) {
        self.init()
        
        switch type {
        case .dateTime: self.dateFormat = "d MMMM, h:mm a"
        case .dateOnly: self.dateFormat = "d MMMM"
        case .timeOnly: self.dateFormat = "h:mm a"
        }
    }
    
    static let dateTimeFormat = DateFormatter(.dateTime)
    static let dateOnlyFormat = DateFormatter(.dateOnly)
    static let timeOnlyFormat = DateFormatter(.timeOnly)
}


