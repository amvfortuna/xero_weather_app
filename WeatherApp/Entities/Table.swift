//
//  Table.swift
//  WeatherApp
//
//  Created by Anna Maria Fortuna on 18/08/19.
//  Copyright © 2019 Anna Fortuna. All rights reserved.
//

import Foundation


struct TableRow {
    var label: String?
    var value: String?
}


struct TableGroup {
    var title: String
    var rows: [TableRow]
}


