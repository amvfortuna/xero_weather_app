//
//  Readings.swift
//  WeatherApp
//
//  Created by Anna Maria Fortuna on 18/08/19.
//  Copyright © 2019 Anna Fortuna. All rights reserved.
//

import Foundation


class Readings: Decodable {
    
    enum ListCodingKeys: String, CodingKey {
        case
        dateTime = "dt",
        dateTimeTxt = "dt_txt",
        mainReading = "main",
        weatherReading = "weather",
        windReading = "wind",
        rain = "rain",
        clouds = "clouds",
        snow = "snow"
        
        enum RainReadingCodingKeys: String, CodingKey {
            case
            volume3h = "3h"
        }
        
        enum CloudReadingCodingKeys: String, CodingKey {
            case
            cloud = "all"
        }
        
        enum SnowReadingCodingKeys: String, CodingKey {
            case
            volume3h = "3h"
        }
    }
    
    var dateTime: TimeInterval?
    var dateTimeText: String?
    var mainReading: MainReadings?
    var weatherReading: [WeatherReadings]?
    var windReading: WindReadings?
    var rainReading: Double?
    var cloudReading: Double?
    var snowReading: Double?
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ListCodingKeys.self)
        self.dateTime = try container.decodeIfPresent(Double.self, forKey: .dateTime)
        self.dateTimeText = try container.decodeIfPresent(String.self, forKey: .dateTimeTxt)
        self.mainReading = try container.decodeIfPresent(MainReadings.self, forKey: .mainReading)
        self.weatherReading = try container.decodeIfPresent([WeatherReadings].self, forKey: .weatherReading)
        self.windReading = try container.decodeIfPresent(WindReadings.self, forKey: .windReading)
        
        if container.contains(.rain) {
            let rainContainer = try container.nestedContainer(keyedBy: ListCodingKeys.RainReadingCodingKeys.self, forKey: .rain)
            self.rainReading = try rainContainer.decodeIfPresent(Double.self, forKey: .volume3h)
        }
        
        if container.contains(.clouds) {
            let cloudContainer = try container.nestedContainer(keyedBy: ListCodingKeys.CloudReadingCodingKeys.self, forKey: .clouds)
            self.cloudReading = try cloudContainer.decodeIfPresent(Double.self, forKey: .cloud)
        }
        
        if container.contains(.snow) {
            let snowContainer = try container.nestedContainer(keyedBy: ListCodingKeys.SnowReadingCodingKeys.self, forKey: .snow)
            self.snowReading = try snowContainer.decodeIfPresent(Double.self, forKey: .volume3h)
        }
    }
}


