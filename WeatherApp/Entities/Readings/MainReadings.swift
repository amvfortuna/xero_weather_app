//
//  MainReadings.swift
//  WeatherApp
//
//  Created by Anna Maria Fortuna on 18/08/19.
//  Copyright © 2019 Anna Fortuna. All rights reserved.
//

import Foundation


class MainReadings: Decodable {
    
    enum MainReadingCodingKeys: String, CodingKey {
        case
        groundLevel = "grnd_level",
        humidity = "humidity",
        pressure = "pressure",
        seaLevel = "sea_level",
        temp = "temp",
        tempMax = "temp_max",
        tempMin = "temp_min"
    }
    
    var groundLevelPressure: Double?
    var humidity: Double?
    var pressure: Double?
    var seaLevelPressure: Double?
    var temperature: Double?
    var maxTemperature: Double?
    var minTemperature: Double?
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: MainReadingCodingKeys.self)
        self.groundLevelPressure = try container.decodeIfPresent(Double.self, forKey: .groundLevel)
        self.humidity = try container.decodeIfPresent(Double.self, forKey: .humidity)
        self.pressure = try container.decodeIfPresent(Double.self, forKey: .pressure)
        self.seaLevelPressure = try container.decodeIfPresent(Double.self, forKey: .seaLevel)
        self.temperature = try container.decodeIfPresent(Double.self, forKey: .temp)
        self.maxTemperature = try container.decodeIfPresent(Double.self, forKey: .tempMax)
        self.minTemperature = try container.decodeIfPresent(Double.self, forKey: .tempMin)
    }
}


