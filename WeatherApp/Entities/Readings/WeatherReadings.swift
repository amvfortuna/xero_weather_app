//
//  WeatherReadings.swift
//  WeatherApp
//
//  Created by Anna Maria Fortuna on 18/08/19.
//  Copyright © 2019 Anna Fortuna. All rights reserved.
//

import Foundation


class WeatherReadings: Decodable {
    
    enum WeatherReadingCodingKeys: String, CodingKey {
        case
        description = "description",
        icon = "icon",
        id = "id",
        main = "main"
    }
    
    var description: String?
    var icon: String?
    var id: Int?
    var main: String?
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: WeatherReadingCodingKeys.self)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id)
        self.description = try container.decodeIfPresent(String.self, forKey: .description)
        self.icon = try container.decodeIfPresent(String.self, forKey: .icon)
        self.main = try container.decodeIfPresent(String.self, forKey: .main)
    }
}


