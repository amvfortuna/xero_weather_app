//
//  WindReadings.swift
//  WeatherApp
//
//  Created by Anna Maria Fortuna on 18/08/19.
//  Copyright © 2019 Anna Fortuna. All rights reserved.
//

import Foundation


class WindReadings: Decodable {
    
    enum WindReadingCodingKeys: String, CodingKey {
        case
        degree = "deg",
        speed = "speed"
    }
    
    var degree: Double?
    var speed: Double?
    var direction: String?
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: WindReadingCodingKeys.self)
        self.degree = try container.decodeIfPresent(Double.self, forKey: .degree)
        self.speed = try container.decodeIfPresent(Double.self, forKey: .speed)
        
        if let windDegree = self.degree {
            switch windDegree {
            case let x where x >= 337.5 || x < 22.5:
                self.direction = "N"
            case let x where x >= 22.5 && x < 67.5:
                self.direction = "NE"
            case let x where x >= 67.5 && x < 112.5:
                self.direction = "E"
            case let x where x >= 112.5 && x < 157.5:
                self.direction = "SE"
            case let x where x >= 157.5 && x < 202.5:
                self.direction = "S"
            case let x where x >= 202.5 && x < 247.5:
                self.direction = "SW"
            case let x where x > 247.5 && x < 292.5:
                self.direction = "W"
            case let x where x > 292.5 && x < 337.5:
                self.direction = "NW"
            default:
                self.direction = nil
            }
        }
    }
}


