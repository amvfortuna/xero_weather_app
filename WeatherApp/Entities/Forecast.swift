//
//  Forecast.swift
//  WeatherApp
//
//  Created by Anna Maria Fortuna on 18/08/19.
//  Copyright © 2019 Anna Fortuna. All rights reserved.
//

import Foundation

class Forecast: Decodable {
    
    enum ForecastCodingKeys: String, CodingKey {
        case
        list = "list"
    }
    
    var list: [Readings]
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ForecastCodingKeys.self)
        self.list = try container.decode([Readings].self, forKey: .list)
    }
}


