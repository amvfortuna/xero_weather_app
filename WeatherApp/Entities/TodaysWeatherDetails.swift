//
//  TodaysWeatherDetails.swift
//  WeatherApp
//
//  Created by Anna Maria Fortuna on 18/08/19.
//  Copyright © 2019 Anna Fortuna. All rights reserved.
//

import Foundation


struct TodaysWeatherDetails {
    var cityName: String
    var dateToday: String
    var mainTemperature: String
    var mainWeather: String
    var mainWeatherIcon: String
    var details: [TableRow]?
    
    init() {
        self.cityName = ""
        self.dateToday = ""
        self.mainTemperature = ""
        self.mainWeather = ""
        self.mainWeatherIcon = ""
    }
}


