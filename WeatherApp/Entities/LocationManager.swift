//
//  LocationManager.swift
//  WeatherApp
//
//  Created by Anna Fortuna on 19/08/19.
//  Copyright © 2019 Anna Fortuna. All rights reserved.
//

import CoreLocation
import CoreData
import UIKit


protocol LocationManagerDelegate: class {
    func locationServiceDenied()
    func locationCaptured(_ cityName: String, country: String)
}

extension LocationManagerDelegate {
    // We can implement this if we need to do something when the location permission was denied.
    // In this app, we can just ignore `denied` authorization status, so this function can be optional.
    func locationServiceDenied() {}
}


class LocationManager: NSObject {
    
    fileprivate(set) var manager: CLLocationManager
    weak var delegate: LocationManagerDelegate?
    
    
    override init() {
        self.manager = CLLocationManager()
        super.init()
        self.manager.delegate = self
    }
    
    
    func requestLocationPermission() {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
                self.manager.requestWhenInUseAuthorization()
            case .authorizedWhenInUse, .authorizedAlways:
                self.manager.requestLocation()
            case .denied, .restricted:
                self.delegate?.locationServiceDenied()
            default:
                break
            }
        }
    }
    
    
    func locationPermissionGranted() -> Bool {
        return CLLocationManager.locationServicesEnabled() && (CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways)
    }
    
    
    class func getDefaultCity() -> City? {
        guard let data = UserDefaults.standard.data(forKey: "DefaultCity"),
            let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext,
            let contextUserInfoKey = CodingUserInfoKey.context else { return nil }
        
        let decoder = JSONDecoder()
        decoder.userInfo[contextUserInfoKey] = context
        return try? decoder.decode(City.self, from: data)
    }
    
    
    class func setDefaultCity(_ city: City) {
        if let encodedCity = try? JSONEncoder().encode(city) {
            UserDefaults.standard.set(encodedCity, forKey: "DefaultCity")
        }
    }
}



// MARK: - Core location delegates
extension LocationManager: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            self.manager.requestWhenInUseAuthorization()
        case .authorizedWhenInUse, .authorizedAlways:
            self.manager.requestLocation()
        case .denied, .restricted:
            self.delegate?.locationServiceDenied()
        default:
            break
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        manager.stopUpdatingLocation()
        print("locationManager didFailWithError: \(error.localizedDescription)")
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            let geoCoder = CLGeocoder()
            
            geoCoder.reverseGeocodeLocation(location, preferredLocale: Locale.current) { [weak self] placemark, error in
                guard let firstPlacemark = placemark?.first,
                    let locality = firstPlacemark.locality,
                    let country = firstPlacemark.isoCountryCode,
                    error == nil else { return }
                
                self?.delegate?.locationCaptured(locality, country: country)
                self?.manager.stopUpdatingLocation()
            }
        }
    }
}


