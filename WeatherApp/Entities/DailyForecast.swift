//
//  DailyForecast.swift
//  WeatherApp
//
//  Created by Anna Maria Fortuna on 18/08/19.
//  Copyright © 2019 Anna Fortuna. All rights reserved.
//

import Foundation


class DailyForecast {
    var date: String
    var threeHourForecasts: [Readings]
    
    init() {
        self.date = ""
        self.threeHourForecasts = [Readings]()
    }
}


