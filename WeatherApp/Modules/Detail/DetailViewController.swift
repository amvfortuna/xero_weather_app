//
//  DetailViewController.swift
//  WeatherApp
//
//  Created by Anna Fortuna on 16/08/19.
//  Copyright © 2019 Anna Fortuna. All rights reserved.
//

import Kingfisher
import JGProgressHUD


class DetailViewController: UIViewController {
    
    var presenter: DetailModulePresenterComponent!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var listContainerView: UIView!
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var mainWeatherLabel: UILabel!
    @IBOutlet weak var mainTemperatureLabel: UILabel!
    @IBOutlet weak var mainWeatherImageView: UIImageView!
    @IBOutlet weak var weatherDetailToggler: UISegmentedControl!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Weather Forecast"

        self.presenter = DetailModulePresenter(delegate: self)
        
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if segue.destination is HourlyForecastTableViewController {
            let dailyForecast = sender as! DailyForecast
            let destination = segue.destination as! HourlyForecastTableViewController
            destination.title = "\(dailyForecast.date) Forecasts"
            destination.dailyForecast = dailyForecast
        }
    }
    
    
    @IBAction func segmentValueChanged(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 1 {
            self.splitViewController?.displayHUD()
            self.presenter.showFiveDayForecast()
        }
        else {
            self.tableView.reloadData()
        }
    }
    
    
    @IBAction func actionButtonTapped(_ sender: UIBarButtonItem) {
        let action = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        action.addAction(UIAlertAction(title: "Set as Default City", style: .default, handler: { _ in
            self.presenter.setDefaultCity()
        }))
        
        action.addAction(UIAlertAction(title: "Add to Favourites", style: .default, handler: { _ in
            self.presenter.addToFavourites()
        }))
        
        action.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        if let popoverController = action.popoverPresentationController {
            popoverController.barButtonItem = sender
        }
        
        self.present(action, animated: true, completion: nil)
    }
}



// MARK: - Table view data source & delegate
extension DetailViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.weatherDetailToggler.selectedSegmentIndex == 0 {
            return self.presenter.todaysWeather?.details?.count ?? 0
        }
        return self.presenter.dailyForecasts?.count ?? 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.weatherDetailToggler.selectedSegmentIndex == 0 {
            let detail = self.presenter.todaysWeather?.details?[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherDetailCell", for: indexPath)
            cell.textLabel?.text = detail?.label
            cell.detailTextLabel?.text = detail?.value
            cell.isUserInteractionEnabled = false
            return cell
        }
        
        let dailyForecast = self.presenter.dailyForecasts?[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "DailyForecastCell", for: indexPath)
        cell.textLabel?.text = dailyForecast?.date
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let dailyForecast = self.presenter.dailyForecasts?[indexPath.row] {
            self.performSegue(withIdentifier: "showHourlyForecast", sender: dailyForecast)
        }
    }
}



// MARK: -
extension DetailViewController: MasterModuleViewDelegate {
    
    func citySelected(_ city: City) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.splitViewController?.displayHUD()
            self.presenter.showTodaysForecast(for: city)
        }
    }
}



// MARK: - Presenter delegates
extension DetailViewController: DetailModulePresenterDelegate {
    
    func displayTodaysWeather() {
        DispatchQueue.main.async {
            self.weatherDetailToggler.selectedSegmentIndex = 0
            self.cityNameLabel.text = self.presenter.todaysWeather?.cityName
            self.dateTimeLabel.text = self.presenter.todaysWeather?.dateToday
            self.mainTemperatureLabel.text = self.presenter.todaysWeather?.mainTemperature
            self.mainWeatherLabel.text = self.presenter.todaysWeather?.mainWeather.uppercased(with: Locale.current)
            
            if let icon = self.presenter.todaysWeather?.mainWeatherIcon {
                let iconURL = URL(string: "https://api.openweathermap.org/img/w/\(icon).png")
                self.mainWeatherImageView.kf.setImage(with: iconURL)
            }
            
            self.tableView.dataSource = self
            self.tableView.delegate = self
            self.tableView.reloadData()
            self.splitViewController?.hideHUD()
            
            self.headerView.isHidden = false
            self.listContainerView.isHidden = false
            
            UIView.animate(withDuration: 0.2, animations: {
                self.headerView.alpha = 1
                self.listContainerView.alpha = 1
            })
        }
    }
    
    
    func displayForecastData() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.splitViewController?.hideHUD()
        }
    }
    
    
    func displayForecastError(_ error: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    func displayHUDMessage(_ message: String) {
        self.splitViewController?.displaySuccessHUD(with: message)
    }
}


