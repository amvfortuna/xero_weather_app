//
//  DetailModuleDefinitions.swift
//  WeatherApp
//
//  Created by Anna Fortuna on 16/08/19.
//  Copyright © 2019 Anna Fortuna. All rights reserved.
//

import Foundation


protocol DetailModulePresenterDelegate: class {
    func displayTodaysWeather()
    func displayForecastData()
    func displayForecastError(_ error: String)
    func displayHUDMessage(_ message: String)
}

protocol DetailModulePresenterComponent: class {
    var dailyForecasts: [DailyForecast]? { get }
    var todaysWeather: TodaysWeatherDetails? { get }
    func setDefaultCity()
    func addToFavourites()
    func showTodaysForecast(for city: City)
    func showFiveDayForecast()
}

protocol DetailModuleInteractorComponent: class {
    func updateDefaultCity() -> Bool
    func updateFavouriteStatus() -> Bool
    func requestCurrentWeatherForecast(for city: City, completion: @escaping (_ forecast: Readings?, _ error: String?) -> Void)
    func requestFiveDayForecaset(completion: @escaping (_ forecast: Forecast?, _ error: String?) -> Void)
}
