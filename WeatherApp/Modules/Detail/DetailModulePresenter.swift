//
//  DetailModulePresenter.swift
//  WeatherApp
//
//  Created by Anna Fortuna on 16/08/19.
//  Copyright © 2019 Anna Fortuna. All rights reserved.
//

import Foundation


class DetailModulePresenter: DetailModulePresenterComponent {
    fileprivate unowned var delegate: DetailModulePresenterDelegate
    fileprivate let interactor: DetailModuleInteractorComponent
    var dailyForecasts: [DailyForecast]?
    var todaysWeather: TodaysWeatherDetails?
    
    
    init(delegate: DetailModulePresenterDelegate) {
        self.delegate = delegate
        self.interactor = DetailModuleInteractor()
    }
    
    
    func setDefaultCity() {
        let updated = self.interactor.updateDefaultCity()
        if updated {
            self.delegate.displayHUDMessage("Your default city has been updated!")
        }
        else {
            self.delegate.displayForecastError("Unable to set this as your default city.")
        }
    }
    
    
    func addToFavourites() {
        let favourite = self.interactor.updateFavouriteStatus()
        if favourite {
            NotificationCenter.default.post(name: NSNotification.Name("RefreshFavourites"), object: nil)
            self.delegate.displayHUDMessage("You've successfully added this city to your favourites!")
        }
        else {
            self.delegate.displayForecastError("Unable to add this city to your favourites.")
        }
    }
    
    
    func showTodaysForecast(for city: City) {
        self.todaysWeather = nil
        self.dailyForecasts = nil
        
        self.interactor.requestCurrentWeatherForecast(for: city) { [weak self] readings, error in
            if error != nil {
                self?.delegate.displayForecastError(error!)
            }
            else {
                self?.prepareTodaysWeatherDetails(readings!, for: city)
            }
        }
    }
    
    
    func showFiveDayForecast() {
        if let forecasts = self.dailyForecasts, !forecasts.isEmpty {
            self.delegate.displayForecastData()
            return
        }
        
        self.interactor.requestFiveDayForecaset { [weak self] forecast, error in
            if error != nil {
                self?.delegate.displayForecastError(error!)
            }
            else {
                self?.prepareFiveDayForecast(forecast!)
            }
        }
    }
}



// MARK: - Private functions
extension DetailModulePresenter {
    
    fileprivate func prepareTodaysWeatherDetails(_ reading: Readings, for city: City) {
        let dateTime = Date(timeIntervalSince1970: reading.dateTime ?? 0)
        let pressure = Measurement(value: reading.mainReading?.pressure ?? 0, unit: UnitPressure.hectopascals)
        let humidity = reading.mainReading?.humidity ?? 0
        
        let minTemp = Measurement(value: reading.mainReading?.minTemperature ?? 0, unit: UnitTemperature.kelvin)
        let maxTemp = Measurement(value: reading.mainReading?.maxTemperature ?? 0, unit: UnitTemperature.kelvin)
        let mainTemperature = Measurement(value: reading.mainReading?.temperature ?? 0, unit: UnitTemperature.kelvin)
        let windSpeed = Measurement(value: reading.windReading?.speed ?? 0, unit: UnitSpeed.metersPerSecond)
        let windDirection = reading.windReading?.direction ?? ""
        
        let dateToday = DateFormatter.dateTimeFormat.string(from: dateTime)
        let convertedMainTemperature = MeasurementFormatter.defaultFormat.string(from: mainTemperature)
        let convertedPressure = MeasurementFormatter.defaultFormat.string(from: pressure)
        let convertedMinTemperature = MeasurementFormatter.defaultFormat.string(from: minTemp)
        let convertedMaxTemperature = MeasurementFormatter.defaultFormat.string(from: maxTemp)
        let convertedWindSpeed = MeasurementFormatter.defaultFormat.string(from: windSpeed)
        
        self.todaysWeather = TodaysWeatherDetails()
        self.todaysWeather!.cityName = "\(city.name!), \(city.country!)"
        self.todaysWeather!.dateToday = dateToday
        self.todaysWeather!.mainTemperature = convertedMainTemperature
        self.todaysWeather!.mainWeather = reading.weatherReading?.first?.description ?? "---"
        self.todaysWeather!.mainWeatherIcon = reading.weatherReading?.first?.icon ?? ""
        self.todaysWeather!.details = [
            TableRow(label: "Pressure", value: convertedPressure),
            TableRow(label: "Humidity", value: "\(humidity) %"),
            TableRow(label: "Min Temperature", value: convertedMinTemperature),
            TableRow(label: "Max Temperature", value: convertedMaxTemperature),
            TableRow(label: "Wind", value: "\(convertedWindSpeed) \(windDirection)")
        ]
        
        self.delegate.displayTodaysWeather()
    }
    
    
    fileprivate func prepareFiveDayForecast(_ forecast: Forecast) {
        self.dailyForecasts = [DailyForecast]()
        
        for reading in forecast.list {
            let dateTime = Date(timeIntervalSince1970: reading.dateTime ?? 0)
            let dateHeader = DateFormatter.dateOnlyFormat.string(from: dateTime)
            
            if self.dailyForecasts!.isEmpty {
                self.dailyForecasts!.append(self.createDailyForecast(from: reading, with: dateHeader))
            }
            else {
                var dailyForecastCreated = false
                innerLoop: for dailyForecast in self.dailyForecasts! {
                    if dailyForecast.date != dateHeader {
                        continue innerLoop
                    }
                    else {
                        dailyForecast.threeHourForecasts.append(reading)
                        dailyForecastCreated = true
                    }
                }
                
                if !dailyForecastCreated {
                    self.dailyForecasts!.append(self.createDailyForecast(from: reading, with: dateHeader))
                }
            }
        }
        
        self.delegate.displayForecastData()
    }
    
    
    fileprivate func createDailyForecast(from reading: Readings, with dateHeader: String) -> DailyForecast {
        let daily = DailyForecast()
        daily.date = dateHeader
        daily.threeHourForecasts.append(reading)
        return daily
    }
}


