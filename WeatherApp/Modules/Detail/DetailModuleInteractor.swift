//
//  DetailModuleInteractor.swift
//  WeatherApp
//
//  Created by Anna Fortuna on 16/08/19.
//  Copyright © 2019 Anna Fortuna. All rights reserved.
//

import Foundation
import Alamofire
import CoreData


class DetailModuleInteractor: DetailModuleInteractorComponent {
    
    fileprivate var city: City?
    
    
    func updateDefaultCity() -> Bool {
        if let selectedCity = self.city {
            LocationManager.setDefaultCity(selectedCity)
            return true
        }
        return false
    }
    
    
    func updateFavouriteStatus() -> Bool {
        if let selectedCity = self.city {
            selectedCity.favourite = true
            let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext
            try? context?.save()
            return true
        }
        return false
    }
    
    
    func requestCurrentWeatherForecast(for city: City, completion: @escaping (_ forecast: Readings?, _ error: String?) -> Void) {
        self.city = city
        
        let url = "https://api.openweathermap.org/data/2.5/weather?id=\(city.id)&appid=359c8b5335974a6385d08ff55679a246"
        
        AF.request(url).responseJSON { [weak self] response in
            self?.processResponse(response, decodeTo: Readings.self, completion: completion)
        }
    }
    
    
    func requestFiveDayForecaset(completion: @escaping (_ forecast: Forecast?, _ error: String?) -> Void) {
        guard let city = self.city else {
            completion(nil, "There is no selected city.")
            return
        }
        
        let url = "https://api.openweathermap.org/data/2.5/forecast?id=\(city.id)&appid=359c8b5335974a6385d08ff55679a246"
        
        AF.request(url).responseJSON { [weak self] response in
            self?.processResponse(response, decodeTo: Forecast.self, completion: completion)
        }
    }
}



// MARK: - Private function
extension DetailModuleInteractor {
    
    func processResponse<D: Decodable>(_ response: DataResponse<Any>, decodeTo decodableType: D.Type, completion: ((_ forecast: D?, _ error: String?) -> Void)) {
        guard let jsonData = response.data else {
            completion(nil, response.error?.localizedDescription)
            return
        }
        
        do {
            let decodableObject = try JSONDecoder().decode(decodableType, from: jsonData)
            return completion(decodableObject, nil)
        }
        catch {
            return completion(nil, error.localizedDescription)
        }
    }
}


