//
//  CityTableViewCell.swift
//  WeatherApp
//
//  Created by Anna Maria Fortuna on 17/08/19.
//  Copyright © 2019 Anna Fortuna. All rights reserved.
//

import Kingfisher


class CityTableViewCell: UITableViewCell {

    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet fileprivate weak var countryFlagImageView: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.cityNameLabel.text = nil
        self.countryFlagImageView.image = nil
    }
    
    
    func showCountryFlag(_ country: String?) {
        guard let countryAbbr = country, !countryAbbr.isEmpty else { return }
        self.countryFlagImageView.image = UIImage(named: "Flags/\(countryAbbr)") ?? UIImage(named: "worldwide")
    }
}
