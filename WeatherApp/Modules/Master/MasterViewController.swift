//
//  MasterViewController.swift
//  WeatherApp
//
//  Created by Anna Fortuna on 16/08/19.
//  Copyright © 2019 Anna Fortuna. All rights reserved.
//

import JGProgressHUD


class MasterViewController: UITableViewController {
    
    fileprivate let searchController = UISearchController(searchResultsController: nil)
    fileprivate var isSearching: Bool = false
    weak var delegate: MasterModuleViewDelegate?
    var presenter: MasterModulePresenterComponent!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Cities"
        
        self.splitViewController?.delegate = self
        self.splitViewController?.preferredDisplayMode = .allVisible
        
        let navController = self.splitViewController?.viewControllers.last as? UINavigationController
        let detailViewController = navController?.topViewController as? DetailViewController
        self.delegate = detailViewController
        
        self.displayLoadingView(true)
        self.presenter = MasterModulePresenter(delegate: self)
        self.presenter.loadCities()
        
        self.searchController.searchBar.delegate = self
        self.searchController.dimsBackgroundDuringPresentation = false
        self.navigationItem.searchController = self.searchController
        self.navigationItem.hidesSearchBarWhenScrolling = false
        self.definesPresentationContext = true
        
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshFavourites), name: NSNotification.Name(rawValue: "RefreshFavourites"), object: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetailView" {
            let navController = segue.destination as! UINavigationController
            let detailViewController = navController.topViewController as! DetailViewController
            self.delegate = detailViewController
            detailViewController.citySelected(sender as! City)
        }
    }
    
    
    @objc fileprivate func refreshFavourites() {
        self.presenter.reloadFavourites()
    }
    
    
    @IBAction func showDefaultCity(_ sender: UIBarButtonItem) {
        if self.presenter.hasDefaultCity() {
            self.presenter.loadDefaultCity()
        }
        else {
            let alert = UIAlertController(title: "No default city.", message: "You haven't set any city as your default city.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isSearching {
            return max(self.presenter.cities.count, 1)
        }
        
        let favourites = self.presenter.favouriteCities?.count ?? 1
        return max(favourites, 1)
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.isSearching {
            guard !self.presenter.cities.isEmpty else {
                return self.configureEmptyCellMessage("Enter a keyword to start searching.", in: tableView, at: indexPath)
            }
            
            let city = self.presenter.cities[indexPath.row]
            return self.configureCityCell(city, in: tableView, at: indexPath)
        }
        
        guard let favourites = self.presenter.favouriteCities, !favourites.isEmpty else {
            return self.configureEmptyCellMessage("You don't have any favourite cities yet. Start searching for a city and add them to your favourites!", in: tableView, at: indexPath)
        }
        
        let city = favourites[indexPath.row]
        return self.configureCityCell(city, in: tableView, at: indexPath)
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.isSearching {
            if self.presenter.cities.indices.contains(indexPath.row) {
                let city = self.presenter.cities[indexPath.row]
                self.navigateToDetailView(with: city)
            }
        }
        else {
            if let favourites = self.presenter.favouriteCities, favourites.indices.contains(indexPath.row) {
                let favouriteCity = favourites[indexPath.row]
                self.navigateToDetailView(with: favouriteCity)
            }
        }
    }
    
    
    fileprivate func configureEmptyCellMessage(_ message: String, in tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NoCityCell", for: indexPath)
        cell.textLabel?.text = message
        return cell
    }
    
    
    fileprivate func configureCityCell(_ city: City, in tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CityCell", for: indexPath) as! CityTableViewCell
        cell.cityNameLabel.text = city.name
        cell.showCountryFlag(city.country)
        return cell
    }
}



// MARK: - Search bar delegate
extension MasterViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.isSearching = true
        self.reloadSearchResults()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.presenter.searchForCity(searchBar.text)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.isSearching = false
        self.reloadSearchResults()
    }
}



// MARK: - Presenter delegates
extension MasterViewController: MasterModulePresenterDelegate {
    
    func displayError(_ error: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Searching error.", message: error, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    func displayLoadingView(_ display: Bool) {
        if display {
            self.splitViewController?.displayHUD()
        }
        else {
            self.splitViewController?.hideHUD()
        }
    }
    
    
    func reloadSearchResults() {
        DispatchQueue.main.async {
            self.displayLoadingView(false)
            self.tableView.reloadData()
        }
    }
    
    
    func promptUserToSetDefaultCity(_ city: City) {
        let name = city.name ?? ""
        let country = city.country ?? ""
        
        let alertController = UIAlertController(title: "Set \(name), \(country) as your default city?", message: "Would you like to set \(name), \(country) as your default city? The next time you open the app, it will show you the weather details of \(name)", preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "Yes", style: .default, handler: { _ in
            self.presenter.setDefaultCity(city)
        }))
        
        alertController.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        
        alertController.addAction(UIAlertAction(title: "No, don't ask again.", style: .destructive, handler: { _ in
            UserDefaults.standard.set(true, forKey: "AskToReplaceDefaultCity")
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func navigateToDetailView(with city: City) {
        self.performSegue(withIdentifier: "showDetailView", sender: city)
    }
}



// MARK: - Split view controller delegate
extension MasterViewController: UISplitViewControllerDelegate {
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        
        return true
    }
}


