//
//  MasterModuleInteractor.swift
//  WeatherApp
//
//  Created by Anna Fortuna on 16/08/19.
//  Copyright © 2019 Anna Fortuna. All rights reserved.
//

import UIKit
import CoreData


class MasterModuleInteractor: MasterModuleInteractorComponent {
    
    fileprivate let managedObjectContext: NSManagedObjectContext
    
    init() {
        self.managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    }
    
    
    func loadCitiesIntoCoreData(completion: @escaping ((_ success: Bool, _ error: String?) -> Void)) {
        // If the `managedObjectContext` failed to fetch the count for the `City`
        // entity for some reason, we just assume that no data was fetched.
        // This means that every time it fails, the app will try to populate/update the
        // `City` entity with data from a JSON file.
        do {
            let count = try self.managedObjectContext.count(for: City.cityFetchRequest())
            if count == 0 {
                self.populateCityEntity { complete in
                    DispatchQueue.main.async {
                        do {
                            try self.managedObjectContext.save()
                            completion(true, nil)
                        }
                        catch {
                            completion(false, error.localizedDescription)
                        }
                        
                    }
                }
            }
            else {
                completion(true, nil)
            }
        }
        catch {
            completion(false, error.localizedDescription)
        }
    }

    
    func requestCities(_ city: String) -> [City]? {
        let request = City.cityFetchRequest()
        request.predicate = NSPredicate(format: "name CONTAINS %@", city)
        request.sortDescriptors = [NSSortDescriptor(key: "country", ascending: true)]
        
        do {
            let cities = try self.managedObjectContext.fetch(request)
            return cities
        }
        catch {
            print("Error in search: \(error.localizedDescription)")
            return nil
        }
    }
    
    
    func retrieveFavouriteCities() -> [City]? {
        let request = City.cityFetchRequest()
        request.predicate = NSPredicate(format: "favourite == true")
        
        do {
            let cities = try self.managedObjectContext.fetch(request)
            return cities
        }
        catch {
            print("Error in search: \(error.localizedDescription)")
            return nil
        }
    }
    
    
    func retrieveCity(_ cityName: String, in country: String) -> City? {
        let request = City.cityFetchRequest()
        request.predicate = NSPredicate(format: "name == %@ AND country == %@", cityName, country)
        request.fetchLimit = 1
        
        do {
            let cities = try self.managedObjectContext.fetch(request)
            return cities.first
        }
        catch {
            print("Error in retrieving city: \(error.localizedDescription)")
            return nil
        }
    }
}



// MARK: - Private functions



extension MasterModuleInteractor {
    
    func populateCityEntity(completion: @escaping (_ done: Bool) -> Void) {
        guard let jsonFilePath = Bundle.main.path(forResource: "city.list.min", ofType: "json", inDirectory: nil),
            let contextUserInfoKey = CodingUserInfoKey.context else { return }
        
        let privateMOC = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        privateMOC.parent = self.managedObjectContext
        
        privateMOC.perform {
            do {
                let jsonData = try Data(contentsOf: URL(fileURLWithPath: jsonFilePath))
                let decoder = JSONDecoder()
                decoder.userInfo[contextUserInfoKey] = privateMOC
                
                _ = try decoder.decode([City].self, from: jsonData)
                try privateMOC.save()
                completion(true)
            }
            catch {
                print("error: ", error)
                completion(false)
            }
        }
    }
}
