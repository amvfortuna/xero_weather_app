//
//  MasterModulePresenter.swift
//  WeatherApp
//
//  Created by Anna Fortuna on 16/08/19.
//  Copyright © 2019 Anna Fortuna. All rights reserved.
//

import Foundation


class MasterModulePresenter: MasterModulePresenterComponent {
    fileprivate unowned var delegate: MasterModulePresenterDelegate
    fileprivate let interactor: MasterModuleInteractorComponent
    fileprivate let locationManager: LocationManager
    var cities: [City]
    var favouriteCities: [City]?
    
    init(delegate: MasterModulePresenterDelegate) {
        self.delegate = delegate
        self.interactor = MasterModuleInteractor()
        self.cities = [City]()
        self.locationManager = LocationManager()
        self.locationManager.delegate = self
    }
    
    
    func hasDefaultCity() -> Bool {
        return UserDefaults.standard.object(forKey: "DefaultCity") != nil
    }
    
    
    func loadDefaultCity() {
        if let defaultCity = LocationManager.getDefaultCity() {
            self.delegate.navigateToDetailView(with: defaultCity)
        }
    }
    
    
    func setDefaultCity(_ city: City) {
        LocationManager.setDefaultCity(city)
        self.delegate.navigateToDetailView(with: city)
    }
    
    
    func loadCities() {
        self.interactor.loadCitiesIntoCoreData { success, error in
            if success {
                // Request for the device's location
                self.locationManager.requestLocationPermission()
                
                // Load the favourite cities
                self.favouriteCities = self.interactor.retrieveFavouriteCities()
                
                self.delegate.reloadSearchResults()
            }
            else {
                self.delegate.displayError(error!)
            }
        }
    }
    
    
    func reloadFavourites() {
        self.favouriteCities = self.interactor.retrieveFavouriteCities()
        self.delegate.reloadSearchResults()
    }
    
    
    func searchForCity(_ city: String?) {
        self.cities.removeAll()
        
        if let searchKeyword = city, !searchKeyword.isEmpty {
            self.cities = self.interactor.requestCities(searchKeyword) ?? [City]()
            self.delegate.reloadSearchResults()
        }
    }
}



// MARK: - Location Manager delegates
extension MasterModulePresenter: LocationManagerDelegate {
    
    func locationCaptured(_ cityName: String, country: String) {
        if !UserDefaults.standard.bool(forKey: "AskToReplaceDefaultCity") {
            guard let city = self.interactor.retrieveCity(cityName, in: country) else { return }
            
            let defaultCity = LocationManager.getDefaultCity()
            if defaultCity == nil || defaultCity!.id != city.id {
                self.delegate.promptUserToSetDefaultCity(city)
            }
        }
    }
}



