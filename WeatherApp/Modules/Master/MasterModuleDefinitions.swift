//
//  MasterModuleDefinitions.swift
//  WeatherApp
//
//  Created by Anna Fortuna on 16/08/19.
//  Copyright © 2019 Anna Fortuna. All rights reserved.
//

import Foundation


protocol MasterModuleViewDelegate: class {
    func citySelected(_ city: City)
}

protocol MasterModulePresenterDelegate: class {
    func reloadSearchResults()
    func displayError(_ error: String)
    func displayLoadingView(_ display: Bool)
    func promptUserToSetDefaultCity(_ city: City)
    func navigateToDetailView(with city: City)
}

protocol MasterModulePresenterComponent: class {
    var cities: [City] { get }
    var favouriteCities: [City]? { get }
    func hasDefaultCity() -> Bool
    func loadDefaultCity()
    func setDefaultCity(_ city: City)
    func loadCities()
    func reloadFavourites()
    func searchForCity(_ city: String?)
}

protocol MasterModuleInteractorComponent: class {
    func loadCitiesIntoCoreData(completion: @escaping ((_ success: Bool, _ error: String?) -> Void))
    func requestCities(_ city: String) -> [City]?
    func retrieveFavouriteCities() -> [City]?
    func retrieveCity(_ cityName: String, in country: String) -> City?
}
