//
//  HourlyForecastModuleDefinitions.swift
//  WeatherApp
//
//  Created by Anna Maria Fortuna on 18/08/19.
//  Copyright © 2019 Anna Fortuna. All rights reserved.
//

import Foundation


protocol HourlyForecastModulePresenterDelegate: class {
    func displayHourlyForecasts()
}

protocol HourlyForecastModulePresenterComponent: class {
    var forecasts: [TableGroup] { get }
    func showHourlyForecast(_ dailyForecast: DailyForecast)
}


