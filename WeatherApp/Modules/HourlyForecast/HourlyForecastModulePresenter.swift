//
//  HourlyForecastModulePresenter.swift
//  WeatherApp
//
//  Created by Anna Maria Fortuna on 18/08/19.
//  Copyright © 2019 Anna Fortuna. All rights reserved.
//

import Foundation


class HourlyForecastModulePresenter: HourlyForecastModulePresenterComponent {
    fileprivate unowned var delegate: HourlyForecastModulePresenterDelegate
    var forecasts: [TableGroup]
    
    
    init(delegate: HourlyForecastModulePresenterDelegate) {
        self.delegate = delegate
        self.forecasts = [TableGroup]()
    }
    
    
    func showHourlyForecast(_ dailyForecast: DailyForecast) {
        self.forecasts = dailyForecast.threeHourForecasts.map {
            self.createForecastGroup(with: $0)
        }
        self.delegate.displayHourlyForecasts()
    }
}



extension HourlyForecastModulePresenter {
    
    func createForecastGroup(with reading: Readings) -> TableGroup {
        let dateTime = Date(timeIntervalSince1970: reading.dateTime ?? 0)
        let groundLevelPressure = Measurement(value: reading.mainReading?.groundLevelPressure ?? 0, unit: UnitPressure.hectopascals)
        let seaLevelPressure = Measurement(value: reading.mainReading?.seaLevelPressure ?? 0, unit: UnitPressure.hectopascals)
        let humidity = reading.mainReading?.humidity ?? 0
        let mainTemperature = Measurement(value: reading.mainReading?.temperature ?? 0, unit: UnitTemperature.kelvin)
        let minTemp = Measurement(value: reading.mainReading?.minTemperature ?? 0, unit: UnitTemperature.kelvin)
        let maxTemp = Measurement(value: reading.mainReading?.maxTemperature ?? 0, unit: UnitTemperature.kelvin)
        let windSpeed = Measurement(value: reading.windReading?.speed ?? 0, unit: UnitSpeed.metersPerSecond)
        let windDirection = reading.windReading?.direction ?? ""
        
        let convertedMainTemperature = MeasurementFormatter.defaultFormat.string(from: mainTemperature)
        let convertedGroundLevelPressure = MeasurementFormatter.defaultFormat.string(from: groundLevelPressure)
        let convertedSeaLevelPressure = MeasurementFormatter.defaultFormat.string(from: seaLevelPressure)
        let convertedMinTemperature = MeasurementFormatter.defaultFormat.string(from: minTemp)
        let convertedMaxTemperature = MeasurementFormatter.defaultFormat.string(from: maxTemp)
        let convertedWindSpeed = MeasurementFormatter.defaultFormat.string(from: windSpeed)
        
        let rows = [
            TableRow(label: "Ground Level Pressure", value: convertedGroundLevelPressure),
            TableRow(label: "Sea Level Pressure", value: convertedSeaLevelPressure),
            TableRow(label: "Humidity", value: "\(humidity) %"),
            TableRow(label: "Temperature", value: convertedMainTemperature),
            TableRow(label: "Min Temperature", value: convertedMinTemperature),
            TableRow(label: "Max Temperature", value: convertedMaxTemperature),
            TableRow(label: "Wind", value: "\(convertedWindSpeed) \(windDirection)")
        ]
        
        return TableGroup(title: DateFormatter.timeOnlyFormat.string(from: dateTime),
                          rows: rows)
    }
}


