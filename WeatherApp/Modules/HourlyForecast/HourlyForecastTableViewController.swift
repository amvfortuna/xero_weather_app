//
//  HourlyForecastTableViewController.swift
//  WeatherApp
//
//  Created by Anna Maria Fortuna on 18/08/19.
//  Copyright © 2019 Anna Fortuna. All rights reserved.
//

import UIKit

class HourlyForecastTableViewController: UITableViewController {
    
    var presenter: HourlyForecastModulePresenterComponent!
    var dailyForecast: DailyForecast!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.presenter = HourlyForecastModulePresenter(delegate: self)
        self.presenter.showHourlyForecast(self.dailyForecast)
        
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.presenter.forecasts.count
    }

    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.forecasts[section].rows.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = self.presenter.forecasts[indexPath.section].rows[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "ForecastDetailCell", for: indexPath)
        cell.textLabel?.text = row.label
        cell.detailTextLabel?.text = row.value
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.presenter.forecasts[section].title
    }
}



// MARK: - Presenter delegate
extension HourlyForecastTableViewController: HourlyForecastModulePresenterDelegate {
    
    func displayHourlyForecasts() {
        self.tableView.reloadData()
    }
}


